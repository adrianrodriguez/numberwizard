﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberWizard : MonoBehaviour {

    // Declare variables in class so that it's available for the entire instance
    // Declare variables in functions and methods if you don't need them for the entire instance
    int max = 1000;
    int min = 1;
    int guess = 500;

	// Use this for initialization
	void Start () {

        // Setting up the max so that it will hit the actual number max
        max = max + 1;

        Debug.Log(min + " " + max + " " + guess);
		Debug.Log("Welcome to number wizard!");
		Debug.Log("Pick a number, don't tell me what it is");
		Debug.Log("The highest number you can pick is: " + max);
		Debug.Log("The lowest number you can pick is: " + min);
        Debug.Log("Tell me if your number is higher or lower than: " + guess);
		Debug.Log("Push UP = HIGHER, Push DOWN = LOWER, Push ENTER = CORRECT!");
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.UpArrow)) {

            // Min is the new guess
            min = guess;
            // New Guess
            myGuess();
            Debug.Log("Is your guess higher or lower than: " + guess);
		} else if(Input.GetKeyDown(KeyCode.DownArrow)) {
            max = guess;
            myGuess();
            Debug.Log("Is your guess higher or lower than: " + guess);
		} else if(Input.GetKeyDown(KeyCode.Return)) {   
            Debug.Log("I've bested you!");
        }
	}

    void myGuess() {
        guess = (max + min) / 2;
    }
}
